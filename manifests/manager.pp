# == Class: tomcat6::manager
#
# Defines the tomcat 6 application manager.
# Note, does not require any dependancies as its handled in this class.
#
# === Parameters
# [*password*]
#   The password for the manager account.
#
# [*user*]
#   The name of the user to access the account.
#   Default: admin
#
# === Variables
#
# === Example
#
class tomcat6::manager(
  $password,
  $user = 'admin',
)
{

  anchor{'tomcat6::manager::begin':
    require => Package['tomcat6'],
  }
  package{'tomcat6-admin':
    ensure  => installed,
    require => Anchor['tomcat6::manager::begin'],
  }

  file {'/etc/tomcat6/tomcat-users.xml':
    ensure  => file,
    group   => 'tomcat6',
    content => "<tomcat-users>\n\
 <user username=\"${user}\" password=\"${password}\"\
 roles=\"manager-gui,admin-gui\"/>\n\
</tomcat-users>\n",
    notify  => Service['tomcat6'],
  }
  anchor{'tomcat6::manager::end':
    require => File['/etc/tomcat6/tomcat-users.xml'],
  }
}
